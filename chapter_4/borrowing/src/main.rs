fn main() {
    let mut s1 = String::from("Hello World!");
    let s2 = s1.clone();

    s1.push_str(" Done!!");

    println!("s1 = {s1} and s2 = {s2}");

    let s = String::from("Hello");

    // We need to use clone, next println gives error because of the "moved" string.
    takes_ownership(s.clone());

    println!("{}", s);

    let x = 5;

    makes_copy(x);

    println!("{x}");

    let _s1 = gives_ownership();

    let s2 = String::from("Hi");

    let s3 = takes_and_gives_back(s2);

    let len = calculate_length(&s3);

    println!("The length of {s3} is {len}");

    let mut change_this_str = String::from("Yello");

    change(&mut change_this_str);

    println!("{change_this_str}");

    let mut str1 = String::from("Hiiii!");

    let r1 = &mut str1;
    // let r2 = &mut str1;

    println!("r1 = {r1}");
    let r2 = &mut str1;

    println!("r2 = {r2}");

    let r3 = &str1;

    println!("r3 = {r3}");

    let some_str = String::from("Hello guys, how are you?");

    let word = first_word(&some_str);

    // some_str.clear(); We can't do that

    println!("First of the some_st is {word}");

    let strr = "Hello, world!";

    let a = [1, 2, 3, 4, 5, 6];

    let sliced_array = &a[..3];

    assert_eq!(sliced_array, &[1, 2, 3]);
}

fn takes_ownership(string: String) {
    println!("{string}");
}

fn makes_copy(integer: i32) {
    println!("{integer}");
}

fn gives_ownership() -> String {
    let some_string = String::from("yours");

    some_string
}

fn takes_and_gives_back(a_string: String) -> String {
    a_string
}

fn calculate_length(s: &String) -> usize {
    s.len()
}

fn change(str: &mut String) {
    str.push_str(" Enddd!")
}

fn first_word(s: &str) -> &str {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[0..i];
        }
    }
    &s[..]
}
