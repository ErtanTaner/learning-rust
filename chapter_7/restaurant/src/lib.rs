// mod front_of_house {
//     mod hosting {
//         fn add_to_waitlist() {}
//         fn seat_at_table() {}
//     }
//
//     mod serving {
//         fn take_order() {}
//         fn serve_order() {}
//         fn take_payment() {}
//     }
// }
//

//
// mod front_of_house {
//     pub mod hosting {
//         pub fn add_to_waitlist() {
//             println!("Added");
//         }
//     }
// }
//
mod front_of_house;

pub use crate::front_of_house::hosting;

pub fn eat_at_restaurant() {
    crate::front_of_house::hosting::add_to_waitlist();

    front_of_house::hosting::add_to_waitlist();
}

fn deliver_order() {}

mod back_of_house {
    fn fix_incorrect_order() {
        cook_order();
        super::deliver_order();
    }

    fn cook_order() {}
}

mod back_of_house2 {
    pub struct Breakfast {
        pub toast: String,
        seasonal_fruit: String,
    }

    impl Breakfast {
        pub fn summer(toast: &str) -> Breakfast {
            Breakfast {
                toast: String::from(toast),
                seasonal_fruit: String::from("peaches"),
            }
        }
    }

    pub enum Appetizer {
        Soup,
        Salad,
    }
}

pub fn eat_at_restaurant2() {
    let mut meal = back_of_house2::Breakfast::summer("Rye");
    meal.toast = String::from("Wheat");

    println!("I'd like {} toast please", meal.toast);

    let x = crate::back_of_house2::Appetizer::Salad;

    match x {
        crate::back_of_house2::Appetizer::Soup => 6,
        crate::back_of_house2::Appetizer::Salad => 12,
    };
}

mod customer {
    use super::front_of_house::hosting;

    pub fn _eat_at_restaurant3() {
        hosting::add_to_waitlist();
    }
}

use std::collections::HashMap;

fn _main() {
    let mut map = HashMap::new();
    map.insert(1, 2);
}

pub use std::cmp::Ordering;
use std::collections::*;
pub use std::fmt::Result;
pub use std::io::Result as IOResult;
pub use std::io::{self, Write};

fn _func1(_x: Result) {}

fn _func2(_x: IOResult<()>) {}
