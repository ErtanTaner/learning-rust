#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle {
    fn area(&self) -> u32 {
        self.width * self.height
    }

    fn width(&self) -> bool {
        self.width > 0
    }

    fn can_hold(&self, rectangle: &Self) -> bool {
        self.width > rectangle.width && self.height > rectangle.height
    }

    fn sqaure(size: u32) -> Self {
        Self {
            width: size,
            height: size,
        }
    }
}

fn main() {
    let width1 = 30;
    let height1 = 50;

    println!(
        "The area of the rectangle is {} square pixels.",
        area(width1, height1)
    );

    let rect1 = (width1, height1);
    println!(
        "The area of the rectangle is {} square pixels.(Used tuple)",
        area2(rect1)
    );

    let rect2 = Rectangle {
        width: width1,
        height: height1,
    };

    println!(
        "The area of the rectangle is {} square pixels.",
        area3(&rect2)
    );

    println!("The rect is = {:#?}", rect2);

    let scale = 2;
    let rect3 = Rectangle {
        width: dbg!(30 * scale),
        height: 50,
    };

    dbg!(&rect3);

    let rect4 = Rectangle {
        width: 30,
        height: 50,
    };

    println!(
        "The area of the rectangle is {} sqaure pixels. (Used method)",
        rect4.area()
    );

    if rect4.width() {
        println!("The rectangle has a nonzero width; it is {}", rect4.width);
    }

    println!("Can rect2 hold rect3? {}", rect2.can_hold(&rect3));
    println!("Can rect3 hold rect4? {}", rect3.can_hold(&rect4));

    let _square = Rectangle::sqaure(16);
}

fn area(width: u32, height: u32) -> u32 {
    width * height
}

fn area2(dimension: (u32, u32)) -> u32 {
    dimension.0 * dimension.1
}

fn area3(rectangle: &Rectangle) -> u32 {
    rectangle.width * rectangle.height
}
