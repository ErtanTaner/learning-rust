struct User {
    active: bool,
    username: String,
    email: String,
    sign_in_count: u64,
}
struct Color(i32, i32, i32);
struct Point(i32, i32, i32);
struct AlwaysEqual;

fn main() {
    let mut user = User {
        active: true,
        username: String::from("someusername123"),
        email: String::from("someone@example.com"),
        sign_in_count: 1,
    };

    user.email = String::from("anotheremail@example.com");

    println!(
        "active = {0}, username = {1}, email = {2}, sign_in_count = {3} ",
        user.active, user.username, user.email, user.sign_in_count
    );

    let user2 = build_user(
        String::from("user2username"),
        String::from("user2email@example.com"),
    );

    println!(
        "active = {0}, username = {1}, email = {2}, sign_in_count = {3}",
        user2.active, user2.username, user2.email, user2.sign_in_count
    );

    let user3 = User {
        active: user.active,
        username: user.username.clone(),
        email: String::from("another@example.com"),
        sign_in_count: user.sign_in_count,
    };

    let user3 = User {
        email: String::from("another2@example.com"),
        ..user
    };

    let black = Color(0, 0, 0);
    let origin = Point(0, 0, 0);

    let subject = AlwaysEqual;
}

fn build_user(username: String, email: String) -> User {
    User {
        active: true,
        username,
        email,
        sign_in_count: 1,
    }
}
