fn main() {
    println!("Which example do you want to use?");
    let mut choice = String::new();
    std::io::stdin()
        .read_line(&mut choice)
        .expect("Wrong input");

    if choice.trim() == "fc" {
        let mut degree = String::new();
        let mut form = String::new();
        println!("Please input a degree");
        std::io::stdin()
            .read_line(&mut degree)
            .expect("Wrong degree input");

        let degree: i32 = match degree.trim().parse() {
            Ok(num) => num,
            Err(_) => panic!("Error while parsing degree to i32"),
        };

        println!("Please input a form for convertion");
        std::io::stdin()
            .read_line(&mut form)
            .expect("Wrong form input");

        let form: char = match form.trim().parse() {
            Ok(char) => char,
            Err(_) => panic!("Error while parsing form to char"),
        };

        let result = fah_to_cels_and_rev(degree, form);
        if result.1 == 'C' {
            println!("Converted Fahrenheit {degree} to Celsius {}", result.0);
        } else if result.1 == 'F' {
            println!("Converted Celsius {degree} to Fahreinheit {}", result.0);
        } else if result.1 == 'N' {
            println!("Wrong input for convertion");
        }
    } else if choice.trim() == "fb" {
        let mut step = String::new();
        println!("Which step you want to see of fibonacci_seq");
        std::io::stdin()
            .read_line(&mut step)
            .expect("Wrong input for fb_seq");

        let step: i32 = match step.trim().parse() {
            Ok(num) => num,
            Err(_) => panic!("Error while parsing form to char"),
        };

        let result = fibonacci_seq(step);
        println!("Result of fibonacci_seq is: {result}");
    } else if choice.trim() == "cc" {
        let mut day = String::new();

        std::io::stdin()
            .read_line(&mut day)
            .expect("Wrong day format");

        let day: usize = match day.trim().parse() {
            Ok(num) => num,
            Err(_) => panic!("Wrong day format to convert"),
        };

        let lyric = christimas_carol(day);
        println!("{day} of the lyric is: {lyric}");
    } else {
        println!("No example with that keyword");
    }
}

fn fah_to_cels_and_rev(degree: i32, choice: char) -> (i32, char) {
    if choice == 'C' && degree < 100 && degree > -273 {
        return ((degree - 32) * 5 / 9, 'C');
    } else if choice == 'F' && degree < 212 && degree > -459 {
        return ((degree * 9 / 5) + 32, 'F');
    }
    (0, 'N')
}

fn fibonacci_seq(step: i32) -> i32 {
    let mut step = step - 1;
    let mut prev = 0;
    let mut current = 1;
    let mut old_current = 0;

    while step != 0 {
        old_current = current;
        current += prev;
        prev = old_current;
        step -= 1;
    }
    current
}

fn christimas_carol(day: usize) -> String {
    let lyrics: [&str; 12] = [
"On the first day of Christmas, my true love gave to me A partridge in a pear tree.",
"On the second day of Christmas, my true love gave to me Two turtle doves, And a partridge in a pear tree.",
"On the third day of Christmas, my true love gave to me Three French hens, Two turtle doves, And a partridge in a pear tree.",
"On the fourth day of Christmas, my true love gave to me Four calling birds, Three French hens, Two turtle doves, And a partridge in a pear tree.",
"On the fifth day of Christmas, my true love gave to me Five golden rings, Four calling birds, Three French hens, Two turtle doves, And a partridge in a pear tree.",
"On the sixth day of Christmas, my true love gave to me Six geese a-laying, Five golden rings, Four calling birds, Three French hens, Two turtle doves, And a partridge in a pear tree.",
"On the seventh day of Christmas, my true love gave to me Seven swans a-swimming, Six geese a-laying, Five golden rings, Four calling birds, Three French hens, Two turtle doves, And a partridge in a pear tree.",
"On the eighth day of Christmas, my true love gave to me Eight maids a-milking, Seven swans a-swimming, Six geese a-laying, Five golden rings, Four calling birds, Three French hens, Two turtle doves, And a partridge in a pear tree.",
"On the ninth day of Christmas, my true love gave to me Nine ladies dancing, Eight maids a-milking, Seven swans a-swimming, Six geese a-laying, Five golden rings, Four calling birds, Three French hens, Two turtle doves, And a partridge in a pear tree.",
"On the tenth day of Christmas, my true love gave to me Ten lords a-leaping, Nine ladies dancing, Eight maids a-milking, Seven swans a-swimming, Six geese a-laying, Five golden rings, Four calling birds, Three French hens, Two turtle doves, And a partridge in a pear tree.",
"On the eleventh day of Christmas, my true love gave to me Eleven pipers piping, Ten lords a-leaping, Nine ladies dancing, Eight maids a-milking, Seven swans a-swimming, Six geese a-laying, Five golden rings, Four calling birds, Three French hens, Two turtle doves, And a partridge in a pear tree.",
"On the twelfth day of Christmas, my true love gave to me Twelve drummers drumming, Eleven pipers piping, Ten lords a-leaping, Nine ladies dancing, Eight maids a-milking, Seven swans a-swimming, Six geese a-laying, Five golden rings, Four calling birds, Three French hens, Two turtle doves, And a partridge in a pear tree!"
];
    let lyric: String = match lyrics[day - 1].parse() {
        Ok(str) => str,
        Err(_) => panic!("Error"),
    };
    lyric
}
