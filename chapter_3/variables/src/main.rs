fn main() {
    let mut x = 5;
    println!("The value of x is: {x}");
    x = 6;
    println!("The value of x is: {x}");

    let tup: (i32, f64, u8) = (64, 6.6, 8);

    let (x, y, z) = tup;
    println!("i32 is: {x} ** f64 is: {y} ** u8 is: {z}");

    let tup2: (i64, f32, u16) = (600, 3.3, 66);
    let xx = tup2.0;
    let yy = tup.1;
    let zz = tup.2;
    println!("First tup2 element is: {xx}");
    println!("Second tup2 element is: {yy}");
    println!("Third tup2 element is: {zz}");

    let first_arr = [6, 7, 1, 2, 12];
    let first_elem_of_arr = first_arr[0];
    println!("First element of the array is: {first_elem_of_arr}");
}
