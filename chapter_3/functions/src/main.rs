// fn main() {
//     println!("Hello, World!");
//     another_function(6, 'E');
// }
//
// fn another_function(x: i32) {
//     println!("Another hello with number: {x}!");
// }
//

fn main() {
    print_labeled_measurement(6, 'h');
    create_scope_block();
    let x = five();
    println!("Return value of the five function is: {x}");
    let res: i32 = plus_one(5);
    println!("Plus one of the 5 is: {res}");
}

fn print_labeled_measurement(value: i32, unit_label: char) {
    println!("The measurement is: {value}{unit_label}");
}

fn create_scope_block() {
    let y = {
        let x = 3;
        x + 1
    };

    println!("The value of scope_block_y is: {y}");
}

fn five() -> i32 {
    5
}

fn plus_one(x: i32) -> i32 {
    x + 1
}
