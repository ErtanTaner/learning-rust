pub fn second_exercise_fn(text: &str) -> String {
    let mut res: Vec<String> = vec![];
    for word in text.split(' ') {
        if is_wovel(&word.chars().next().expect("Empty")) {
            res.push(format!("{}-hay", &word.trim()));
        } else {
            let without_first_char = &word[1..];
            let first_char = word.chars().next().expect("Empty");
            res.push(format!("{}{}-ay", &without_first_char, &first_char));
        }
    }
    res.join(" ")
}

fn is_wovel(char: &char) -> bool {
    let vowels: [char; 8] = ['a', 'e', 'ı', 'i', 'u', 'ü', 'o', 'ö'];
    vowels.contains(char)
}
