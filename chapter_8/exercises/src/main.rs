mod first_exercise;
mod second_exercise;
mod third_exercise;

use std::io;

fn main() {
    loop {
        println!("Choose");
        let mut choice = String::new();
        io::stdin()
            .read_line(&mut choice)
            .expect("There is problem with your input");
        let choice = &choice[..];
        println!("{}", &choice);
        match choice.trim() {
            "1" => {
                let first_exercise_result =
                    first_exercise::first_exercise_fn(&mut [1, 2, 3, 4, 6, 5, 1, 1, 1, 1]);
                println!("First exercise result is: {:#?}", first_exercise_result);
            }
            "2" => {
                let mut user_choice = String::new();
                io::stdin()
                    .read_line(&mut user_choice)
                    .expect("Something went wrong.");
                let second_exercise_result = second_exercise::second_exercise_fn(&user_choice[..]);
                println!("Second exercise result is: {:#?}", &second_exercise_result);
            }
            "3" => {
                third_exercise::third_exercise_fn();
                println!("Third exercise result is done!");
            }
            &_ => continue,
        }
    }
}
