use std::collections::HashMap;

pub fn first_exercise_fn(numbers: &mut [i32]) -> (i32, i32) {
    numbers.sort();
    let median: i32;
    if numbers.len() % 2 == 0 {
        median = (numbers[numbers.len() / 2] + numbers[numbers.len() / 2 + 1]) / 2;
    } else {
        median = numbers[numbers.len()];
    }
    let mut mode_map = HashMap::new();

    for num in numbers {
        let counter = mode_map.entry(num).or_insert(0);
        *counter += 1;
    }

    let mut most_used = 0;
    let mut most_used_count = 0;

    for (num, count) in mode_map {
        if most_used_count < count {
            most_used = *num;
            most_used_count = count;
        }
    }

    (median, most_used)
}
