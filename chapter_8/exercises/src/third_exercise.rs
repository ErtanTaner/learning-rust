use std::{collections::HashMap, io};
use uuid::Uuid;

pub fn third_exercise_fn() {
    let mut company: HashMap<String, Vec<Employee>> = HashMap::new();
    loop {
        println!("What do you want to do?\n1. add\n2. get all employees in a department\n3. get all employees in company\n4. close");
        let action: u32 = match read_return().parse() {
            Ok(res) => res,
            Err(err) => {
                println!("{:#?}", err);
                0
            }
        };

        match Action::decide(&action) {
            Action::Add => {
                let mut depart: Vec<Employee> = vec![];
                println!("Which department you want to add?");
                let department = read_return();
                println!("Name:");
                let name = read_return();
                depart.push(Employee::new(&name, Uuid::new_v4().to_string()));
                match company.get(&department) {
                    Some(emps) => {
                        for emp in emps {
                            depart.push(Employee::new(&emp.name, Uuid::new_v4().to_string()));
                        }
                        company.insert(String::from(&department), depart);
                    }
                    None => {
                        company.insert(String::from(&department), depart);
                    }
                }
            }
            Action::DGetAll => {
                println!("Which department you want to see?");
                let choice = read_return();
                match company.get(&choice) {
                    Some(emps) => {
                        println!("Employees in the {choice}");
                        for emp in emps {
                            println!("{:#?}", emp);
                        }
                    }
                    None => println!("This department is empty!"),
                };
            }
            Action::CGetAll => {
                println!("Every employee in company");
                for (dep, emps) in &company {
                    println!("{dep} employees");
                    for emp in emps {
                        println!("{:#?}", emp);
                    }
                }
            }
            Action::Close => {
                println!("Thank you for using this terminal tool to interact with our company!");
                break;
            }
        }
    }
}

#[derive(Debug)]
struct Employee {
    id: String,
    name: String,
}

impl Employee {
    pub fn new(name: &str, id: String) -> Self {
        Self {
            name: String::from(name),
            id,
        }
    }
}

enum Action {
    Add,
    DGetAll,
    CGetAll,
    Close,
}

impl Action {
    pub fn decide(action: &u32) -> Self {
        match action {
            1 => Action::Add,
            2 => Action::DGetAll,
            3 => Action::CGetAll,
            _ => Action::Close,
        }
    }
}

fn read_return() -> String {
    let mut inp = String::new();

    io::stdin()
        .read_line(&mut inp)
        .expect("Something happened while reading input.");
    String::from(inp.trim())
}
