fn main() {
    let mut s = String::new();
    let mut h = String::from("Selam iöü");
    let s2 = "bar";
    s.push_str(s2);
    println!("s2 is {s2}");
    let mut s = String::from("lo");
    s.push('l');
    let s1 = String::from("Hello, ");
    let s2 = String::from("world!");
    let s3 = s1 + &s2;
    println!("{s2}");

    let s1 = String::from("tic");
    let s2 = String::from("tac");
    let s3 = String::from("toe");

    // Normal way
    //let s = s1 + "-" + &s2 + "-" + &s3;
    let s = format!("{s1}-{s2}-{s3}");
    println!("{s1}, {s2}, {s3}");
    println!("{}", &s[..1]);
    for c in "hello".chars() {
        println!("{c}");
    }
    for b in "helo".bytes() {
        println!("{b}");
    }
}
