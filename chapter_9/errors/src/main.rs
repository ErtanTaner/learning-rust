use std::{
    error::Error,
    fs::{self, File},
    io::{self, ErrorKind, Read},
};

fn main() -> Result<(), Box<dyn Error>> {
    let greeting_file_result = File::open("hello.txt");
    let _file = match greeting_file_result {
        Ok(file) => file,
        Err(error) => match error.kind() {
            ErrorKind::NotFound => match File::create("hello.txt") {
                Ok(fc) => fc,
                Err(error) => panic!("Problem creating the file: {:?}", error),
            },
            other_error => {
                panic!("Problem opening the file: {:?}", other_error);
            }
        },
    };
    //let greeting_file = File::open("hello6.txt").unwrap();
    let _greeting_file2 =
        File::open("hello6.txt").expect("hello6.txt should be included in this project");

    let _greeting_file = File::open("hello6.txt")?;
    Ok(())
}

fn _read_username() -> Result<String, io::Error> {
    let username_file_result = File::open("hello6.txt");

    let mut username_file = match username_file_result {
        Ok(file) => file,
        Err(e) => return Err(e),
    };

    let mut username = String::new();

    match username_file.read_to_string(&mut username) {
        Ok(_) => Ok(username),
        Err(e) => Err(e),
    }
}

fn _read_username_from_file() -> Result<String, io::Error> {
    let mut username_file = File::open("hello12.txt")?;
    let mut username = String::new();

    username_file.read_to_string(&mut username)?;
    Ok(username)
}

fn _read_username_from_file2() -> Result<String, io::Error> {
    let mut username = String::new();

    File::open("hello6.txt")?.read_to_string(&mut username)?;

    Ok(username)
}

fn _read_username_from_file3() -> Result<String, io::Error> {
    fs::read_to_string("hello6.txt")
}

fn _last_char_of_first_line(text: &str) -> Option<char> {
    text.lines().next()?.chars().last()
}
